﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public float vely;
    private float moveVertical;
    private Vector3 positionActual;
    public float maxy = 8;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        moveVertical = Input.GetAxis("Vertical");

       positionActual = transform.position;

         if(moveVertical >0)
        {
            positionActual.y = positionActual.y + vely * moveVertical;
        }

         if (moveVertical < 0)
        {
            positionActual.y = positionActual.y + vely * moveVertical;
         
        }
        

        if(positionActual.y > maxy)
        {
            positionActual.y = maxy;
        }


        else if(positionActual.y <-maxy)
        {
            positionActual.y = -maxy;
        }
        transform.position = positionActual;
    }

}
